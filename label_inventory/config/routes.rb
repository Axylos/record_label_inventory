LabelInventory::Application.routes.draw do

  resources :users do
    collection do
      get 'activate'
    end
  end
  resource :session, only: [:create, :destroy, :new]

  resources :bands do
    resources :albums, only: [:new, :create, :index]
    resources :tracks, only: [:new, :create, :index]
  end

  resources :tracks, only: [:show, :destroy, :update, :edit] do
    resources :notes, only: [:show, :new, :create]
  end

  resources :notes, only: [:destroy]
  resources :albums, only: [:show, :destroy, :update, :edit]

  root to: 'bands#index'
end
