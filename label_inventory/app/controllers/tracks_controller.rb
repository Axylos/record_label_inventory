class TracksController < ApplicationController

  def new
    @band = Band.find params[:band_id]
    @track = Track.new
  end

  def create
    @track = Track.new(track_params)
    if @track.save
      redirect_to track_url(@track)
    else
      flash[:errors] = @track.errors.full_messages
      redirect_to new_band_track_url
    end
  end

  def index
    @track = Track.find(params[:id])
  end

  def update
    @track = Track.find(params[:id])
    if @track.update(track_params)
      render :show
    else
      flash[:errors] = @track.errors.full_messages
      redirect_to edit_track_url(@track)
    end
  end

  def show
    @track = Track.find params[:id]
  end

  def edit
    @track = Track.find params[:id]
    @album = @track.album
    @band = @track.band
  end

  def destroy
    track = Track.find params[:id]
    @album = track.album
    track.destroy
    redirect_to album_url(@album)
  end


  private

  def track_params
    params.require(:track).permit(:title, :lyrics, :album_id, :status)
  end
end
