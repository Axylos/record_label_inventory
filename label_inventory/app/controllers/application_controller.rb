class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def login_user!(user)
    token = user.reset_session_token!

    session[:token] = token
  end

  def current_user
    if session[:token]
      User.find_by_session_token(session[:token])
    end
  end

  def logged_in?
    current_user.present?
  end
end
