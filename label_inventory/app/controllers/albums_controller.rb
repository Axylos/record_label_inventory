class AlbumsController < ApplicationController

  def new
    @album = Album.new
    @band = Band.find(params[:band_id])
    @bands = Band.all
  end

  def create
    @album = Album.new(album_params)
    if @album.save
      redirect_to album_url(@album)
    else
      flash[:errors] = @album.errors.full_messages
      redirect_to new_band_album_url
    end
  end

  def index
    @albums = Album.all
  end

  def show
    @album = Album.find(params[:id])
  end

  def edit
    @bands = Band.all
    @album = Album.find params[:id]
    @band = @album.band
  end

  def update
    @album = Album.find(params[:id])
    @album.update(album_params)

    render :show
  end

  def destroy
    @album = Album.find(params[:id])
    @band = @album.band
    @album.destroy
    redirect_to band_url(@band)
  end


  private

  def album_params
    params.require(:album).permit(:band_id, :title, :setting)
  end
end
