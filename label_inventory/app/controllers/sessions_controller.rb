class SessionsController < ApplicationController

  def new
  end

  def create
    @user = User.find_by_credentials(
      user_params[:email],
      user_params[:password]
    )

    if @user
      login_user!(@user)
      redirect_to user_url(@user)
    else
      flash.now[:errors] = "Invalid Credentials"
      render :new
    end
  end

  def destroy
    @user = User.find_by_session_token(session[:token])
    logout!(@user)
    redirect_to new_session_url
  end


  private

  def logout!(user)
    user.reset_session_token!
    session[:token] = nil

  end

  def user_params
    params.require(:user).permit(:email, :password)
  end
end
