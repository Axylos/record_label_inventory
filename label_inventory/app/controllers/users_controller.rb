class UsersController < ApplicationController
  before_action :ensure_login, only: :index

  def index
    @users = User.all
  end

  def activate
    fail
  end

  def new
  end

  def show
    @user = User.find(params[:id])
  end

  def create
    @user = User.new(user_params)
    if @user.save!
      msg = UserMailer.welcome_email(@user)
      #msg.deliver
      login_user!(@user)
      redirect_to user_url(@user)
    else
      flash.now[:errors] = "Sign up failed!"
      render new_user_url
    end
  end


  private

  def user_params
    params.require(:user).permit(:email, :password)
  end

  def ensure_login
    redirect_to new_user_url unless logged_in?
  end
end
