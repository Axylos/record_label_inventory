class NotesController < ApplicationController
  before_action :confirm_author, only: [:destroy]

  def new
    @note = Note.new
  end

  def create
    @note = Note.new(note_params)
    @note.user_id = current_user.id
    if @note.save
      redirect_to track_url(@note.track)
    else
      flash[:errors] = @note.errors.full_messages
      redirect_to track_url(@note.track)
    end
  end

  def destroy
    @note = Note.find(params[:id])
    @track = @note.track
    @note.destroy
    redirect_to track_url(@track)
  end


  private

  def note_params
    params.require(:note).permit(:body).merge(
    params.permit(:track_id)
    )
  end

  def confirm_author
    unless current_user == Note.find(params[:id]).author
      flash[:errors] = "Keep your paws off other users' stuff!"
      redirect_to track_url(Note.find(params[:id]).track)
    end
  end
end
