module ApplicationHelper

  def current_user
    if session[:token]
      User.find_by_session_token(session[:token])
    end
  end

  def logout!(user)
    user.reset_session_token!
    session[:token] = nil

  end

end
