class User < ActiveRecord::Base
  attr_reader :password
  has_many :notes, class_name: "Note", foreign_key: :user_id

  validates :session_token, :email, :password_digest, presence: true
  validates :password, length: { minimum: 6 }
  before_validation :ensure_session_token
  before_validation :ensure_validation_token



  def self.generate_token
    SecureRandom.urlsafe_base64(16)
  end

  def reset_session_token!
    token = User.generate_token
    update_attribute(:session_token, token)
    token
  end

  def password=(secret)
    if secret.present?
      @password = secret
    end
      pass_digest = BCrypt::Password.create(secret)
      write_attribute(:password_digest, pass_digest)
  end

  def is_password?(secret)
    BCrypt::Password.new(self.password_digest).is_password?(secret)
  end

  def self.find_by_credentials(email, secret)
    user = User.find_by_email(email)
    return if user.nil?

    if user.is_password? secret
      user
    else
      nil
    end
  end


  private

  def ensure_session_token

    unless self.session_token
      reset_session_token!
    end
  end

  def ensure_validation_token
    unless self.activation_token
      self.activation_token = User.generate_token

    end
  end

end