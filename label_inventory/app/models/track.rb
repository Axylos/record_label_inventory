class Track < ActiveRecord::Base
  validates :title, :album_id, presence: true
  validates :title, uniqueness: { scope: :album_id }
  validates :status, inclusion: { in: %w{ Regular Bonus } }
  belongs_to :album
  has_many :notes

  delegate :band, to: :album

end