class Album < ActiveRecord::Base
  validates :title, :band_id, :setting, presence: true
  validates :setting, inclusion: { in: %w{ Live Studio } }
  belongs_to :band
  has_many :recordings, class_name: "Track", foreign_key: :album_id
  validates :title, uniqueness: { scope: :band_id }
end