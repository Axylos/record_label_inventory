class AddTitleToAlbum < ActiveRecord::Migration
  def change
    add_column :albums, :title, :string, null: false
  end
end
