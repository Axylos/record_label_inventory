class CreateTrack < ActiveRecord::Migration
  def change
    create_table :tracks do |t|
      t.integer :album_id, null: :false
      t.string :status, null: false
      t.text :lyrics, null: false

      t.timestamps
    end
    add_index :tracks, :album_id
  end
end
