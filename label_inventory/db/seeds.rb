tool = Band.create!(name: "Tool")
metallica = Band.create!(name: "Metallica")

aenima = Album.create!(band_id: 1, setting: "Studio", title: "Aenima")
opiate = Album.create!(band_id: 1, setting: "Live", title: "Opiate")

master_puppets = Album.create(
  band_id: 2,
  setting: "Studio",
  title: "Master of Puppets"
)

pushit = Track.create!(
  album_id: 1,
  status: "Regular",
  title: "Pushit",
  lyrics: "Stuff"
)

battery = Track.create!(album_id: 3, status: "regular", title: "Battery",
    status: "Bonus", lyrics: "angry stuff")